//
//  FireSearchViewController.swift
//  Fire Search
//
//  Created by Meikell Lamarr on 6/24/15.
//  Copyright © 2015 Ember. All rights reserved.
//

import Cocoa
import WebKit

enum InputError: ErrorType
{
    case UnparseableString
}

extension String: SequenceType
{
    func isValidInput(s: String) -> Bool {
        for x in self {
            for y in s {
                if x == y {
                    return true
                }
            }
        }
        return false
    }
}

class FireSearchViewController: NSViewController
{
    
    @IBOutlet weak var webView: WebView!
    @IBOutlet weak var textLabel: NSTextField!
    @IBOutlet weak var textField: NSTextField!
    @IBOutlet weak var goButton: NSButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.textField.delegate = self
    }
    
    override var representedObject: AnyObject?
        {
        didSet
        {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func goButtonPressed(sender: NSButton) -> Void
    {
        guard !self.textField.stringValue.isEmpty else {
            return
        }
        
        guard self.textField.stringValue.utf16.count < 20 else {
            return
        }
        
        do {
            try self.parseInput(self.textField.stringValue)
        }
        catch InputError.UnparseableString {
            print("String can not be parsed, incorrect format.")
        }
        catch {
            print("Error")
        }
    }
    
    func parseInput(stdin: String) throws -> Void
    {
        guard stdin.isValidInput("!=-()@!#$%^<>?[]{}+") == false else {
            throw InputError.UnparseableString
        }
        
        let formattedString = self.textLabel.stringValue.stringByReplacingOccurrencesOfString(" ", withString: "+")
        
        let urlString = "https://www.google.com/search?q=\(formattedString)"
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringCacheData, timeoutInterval: 6.0)
        self.webView.mainFrame.loadRequest(request)
        
        self.textLabel.hidden = true
        self.textField.hidden = true
        self.goButton.hidden = true
    }
}

extension FireSearchViewController: NSTextFieldDelegate
{
    func control(control: NSControl, textShouldBeginEditing fieldEditor: NSText) -> Bool {
        print("begin")
        return true
    }
    override func controlTextDidChange(obj: NSNotification) {
        self.textLabel.stringValue = ""
        self.textLabel.stringValue = self.textField.stringValue
    }
    func control(control: NSControl, textShouldEndEditing fieldEditor: NSText) -> Bool {
        print("ending")
        return true
    }
}
